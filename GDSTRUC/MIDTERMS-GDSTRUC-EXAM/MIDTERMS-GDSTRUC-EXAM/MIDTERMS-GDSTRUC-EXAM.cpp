#include "pch.h"
#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;

void main()
{
	int loop = 0;
	int choice;
	cout << "Enter size of array: ";
	int size;
	cin >> size;

	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}

	cout << "\nGenerated array: " << endl;
	cout << "Unordered: ";
	for (int i = 0; i < unordered.getSize(); i++)
		cout << unordered[i] << "   ";
	cout << "\nOrdered: ";
	for (int i = 0; i < ordered.getSize(); i++)
		cout << ordered[i] << "   ";

	cout << "\n\nEnter number to search: ";
	int input;
	cin >> input;
	cout << endl;

	while (loop = 1)
	{
		cout << "What do you wanna do?" << endl;
		cout << "1 - Remove element at index " << endl;
		cout << "2 - Search for element " << endl;
		cout << "3 - Expand and generate random values" << endl;
		cout << "4 - Suicide" << endl;
		cin >> choice;

		if (choice == 1)
		{
			cout << "Which index do you wanna remove?" << endl;
			int index;
			cin >> index;
			unordered.remove(index);
			ordered.remove(index);
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
		}

		if (choice == 2)
		{
			//search
			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);

			if (result >= 0)
			{
				cout << input << " was found at index " << result << ".\n";
			}
			else
			{
				cout << input << " not found." << endl;
				system("pause");

			}
		}

		if (choice == 3)
		{
			int expansion;
			cout << "Input Size of expansion" << endl;
			cin >> expansion;

			for (int i = 0; i < expansion; i++)
			{
				int rng = rand() % 101;
				unordered.push(rng);
				ordered.push(rng);
			}

			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";

			cout << endl;
		}

		if (choice == 4)
		{
		cout << R"( _____       _      _     _      
/  ___|     (_)    (_)   | |     
\ `--. _   _ _  ___ _  __| | ___ 
 `--. \ | | | |/ __| |/ _` |/ _ \
/\__/ / |_| | | (__| | (_| |  __/
\____/ \__,_|_|\___|_|\__,_|\___|)" << endl;
			break;
		}
	}
	system("pause");
}