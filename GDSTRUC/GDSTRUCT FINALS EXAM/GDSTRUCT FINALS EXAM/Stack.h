#pragma once
#include"UnorderedArray.h"
#include<cstdlib>
#include<iostream>
#include<string>

using namespace std;

template<class T>
class Stack
{
public:
	Stack(int size)
	{
		mContainer = new UnorderedArray<T>(size);
	}

	void display()
	{
		for (int i = (*mContainer).getSize(); i > 0; i--)
		{
			cout << (*mContainer)[i - 1] << endl;
		}
	}

	void removeAll()
	{
		for (int i = (*mContainer).getSize(); i > 0; i--)
		{
			mContainer->remove(i - 1);
			mContainer->pop();
		}
	}

	void push(T value)
	{
		mContainer->push(value);
	}

	T top()
	{
		return (*mContainer)[(*mContainer).getSize() - 1];
	}

	void pop()
	{
		mContainer->remove((*mContainer).getSize() - 1);
		mContainer->pop();
	}

private:
	UnorderedArray<T>*mContainer;
};

