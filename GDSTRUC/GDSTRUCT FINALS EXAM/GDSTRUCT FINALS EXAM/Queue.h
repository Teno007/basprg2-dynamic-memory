#pragma once
#include"UnorderedArray.h"
#include<cstdlib>
#include<iostream>
#include<string>

using namespace std;

template<class T>
class Queue
{
public:
	Queue(int size)
	{
		mContainer = new UnorderedArray<T>(size);
	}

	void display()
	{
		for (int i = 0; i < (*mContainer).getSize(); i++)
		{
			cout << (*mContainer)[i] << endl;
		}
	}

	void push(T value)
	{
		mContainer->push(value);
	}

	void pop()
	{
		mContainer->remove(0);
		mContainer->pop();
	}



	void removeAll()
	{
		for (int i = 0; i <= (*mContainer).getSize(); i++)
		{
			mContainer->remove(i);
			mContainer->pop();
		}
	}

	T top()
	{
		return (*mContainer)[0];
	}
private:
	UnorderedArray<T>*mContainer;
};

