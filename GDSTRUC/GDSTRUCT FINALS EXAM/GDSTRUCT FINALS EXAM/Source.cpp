#include<iostream>
#include"Queue.h"
#include"Stack.h"
#include<cstdlib>

using namespace std;

int main()
{
	int size;
	int choice;
	int num;
	int loop = 1;

	cout << "Enter size for element sets:";
	cin >> size;

	Queue<int>Queue(size);
	Stack<int>Stack(size);

	while (loop == 1)
	{
		cout << "What do you want to do?" << endl;
		cout << "1. Push" << endl;
		cout << "2. Pop" << endl;
		cout << "3. Print then empty set" << endl;
		cin >> choice;

		if (choice == 1)
		{
			cout << "enter number:";
			cin >> num;
			Queue.push(num);
			Stack.push(num);
		}

		else if (choice == 2)
		{
			cout << "popping first element;" << endl;
			Queue.pop();
		}

		else if (choice == 3)
		{
			cout << "queue elements:" << endl;
			Queue.display();
			cout << "stacking elements;" << endl;
			Stack.display();
			Queue.removeAll();
			Stack.removeAll();
		}


		system("pause");
		system("cls");
	}
}