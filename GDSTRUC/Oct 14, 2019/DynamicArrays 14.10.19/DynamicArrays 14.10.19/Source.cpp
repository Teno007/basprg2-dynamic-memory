#include <iostream>
#include <string>
#include <vector>
#include "UnorderedArray.h"
using namespace std;

void main()
{
	UnorderedArray<int> grades(5);

	int choice;
	int n = 1;

	cout << "enter the size of array" << endl;
	cin >> choice;

	for (int i = 1; i <= choice; i++)
	{
		int randnum = rand() % 100 + 1;
		grades.push(randnum);
	}
	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << "(" << n << ")  ";
		cout << grades[i] << endl;
		n++;
	}

	system("pause");
	
	cout << "You gotta remove one of the elements.. which one?" << endl;
	cin >> choice;
	choice--;
	grades.remove(choice);

	system("cls");

	int j = 1;

	for (int i = 0; i < grades.getSize(); i++)
	{
		cout << "(" << j << ")  ";
		cout << grades[i] << endl;
		j++;
	}

	cout << "please search a element" << endl;
	cin >> choice;

	for (int i = 0; i < grades.getSize(); i++)
	{
		if (choice == grades[i])
		{
			cout << "element has been found" << endl;
			cout << grades[i] << endl;
		}

		if (i > grades.getSize())
		{
			cout << "element has not been found" << endl;
		}
	}


	system("pause");
}