#include <iostream>
#include <string>

using namespace std;

void addmember(string guildmember[], int guildsize)
{
	int n = 1;
	for (int i = 0; i < guildsize; i++)
	{
		cout << "Name of your " << n << " member." << endl;
		cin >> guildmember[i];
		n++;
		system("cls");
	}
}

void printmembers(string guildmember[], string guildname, int guildsize)
{
	int n = 1;
	cout << "In the " << guildname << " Guild." << endl;
	for (int i = 0; i < guildsize; i++)
	{
		cout << "(" << n << ")" << guildmember[i] << endl;
		n++;
	}
	cout << endl;
}


int main()
{
	string guildname;
	int guildsize;
	int iL = 0;

	cout << "Hi welcome to Guild Maker" << endl;
	cout << "Type the name for the Guild you want to make." << endl;
	cin >> guildname;

	cout << "How many people you want in your guild?" << endl;
	cin >> guildsize;

	string* guildmember = new string[guildsize];

	system("cls");

	cout << "Your guild name is " << guildname << endl;
	cout << "And the size of your guild is " << guildsize << endl;

	addmember(guildmember, guildsize);

	while (iL = 1)
	{
		int choice;
		char newname;

		printmembers(guildmember, guildname, guildsize);
		cout << "What do you wanna do?" << endl;
		cout << "(1) Rename a member" << endl;
		cout << "(2) Add a member" << endl;
		cout << "(3) Delete a member" << endl;
		cout << "(4) Die" << endl;
		cout << endl;
		cin >> choice;
		system("cls");
		if (choice == 1)
		{
			printmembers(guildmember, guildname, guildsize);
			cout << endl;
			cout << "Who do you wanna rename?" << endl;
			cin >> choice;
			choice--;
			cout << guildmember[choice] << " into ";
			cin >> newname;
			guildmember[choice] = newname;
		}
		else if (choice == 2)
		{
			int m = guildsize + 1;
			cout << "Name of your " << m << " member." << endl;
			cin >> guildmember[m];
		}
		else if (choice == 3)
		{
			printmembers(guildmember, guildname, guildsize);
			cout << endl;
			cout << "Who do you wanna delete?" << endl;
			cin >> choice;
			choice--;
			guildmember[choice] = "Empty Slot";
		}
		else if (choice == 4)
		{
			system("cls");
			cout << "My life is meaningless without you, bye bye" << endl;
			break;
		}
	}

	system("pause");
	return 0;
}