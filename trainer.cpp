#include "trainer.h"
#include <iostream>
#include <string>
#include <time.h>
#include "pokemon.h"

pokemon* Pokemon = new pokemon;

using namespace std;

int r = rand() % 15 + 1;

trainer::trainer()
{
	this->name = "";
	this->x;
	this->y;
}

trainer::trainer(string name)
{
	this->name = name;
}

void trainer::getName(string name)
{
	cout << "Hi, trainer! What is your name?" << endl;
	cin >> this->name;
	cout << endl;
}

void trainer::starterpokemon()
{
	cout << "Its so nice to finally meet you " << endl;
	cout << "I have three pokemons here with me." << endl;
	cout << "You can have one!! Go on, Choose!" << endl << endl << endl;
	cout << "[1] Bulbasaur       Grass Type   Level 5" << endl;
	cout << "[2] Charmander      Fire Type    Level 5" << endl;
	cout << "[3] Squirtle        Water Type   Level 5" << endl;
	cin >> input;
	system("pause");
	system("cls");
	if (input == 1)
	{
		cout << "You have chosen Bulbasaur" << endl;
		pokemonint[0] = pokemonlist[0];
	}
	else if (input == 2)
	{
		cout << "You have chosen Charmander" << endl;
		pokemonint[0] = pokemonlist[1];
	}
	else if (input == 3)
	{
		cout << "You have chosen Squirtle" << endl;
		pokemonint[0] = pokemonlist[2];
	}
	pokemonintnum++;
	system("pause");
	system("cls");
}


void trainer::move(int x, int y, int OFF, float encounterrate)
{
	cout << "Which region do you want to start in?" << endl;
	cout << R"(-10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0  1  2  3  4  5  6  7  8  9  10
 9    MT   |                   |                    |VERIDIAN 9
 8  MOON   |                   | /\>>>>>>>>>>>>>>>>>    CITY  8
 7 ____  _(1)                  | /\                 (2)______ 7
 6     /\                      | /\                           6
 5     /\                      | /\                           5
 4     /\                      | /\                           4
 3     /\                  (3)_|_/\                           3
 2     /\                  | PALLET|                          2
 1     <<<<<<<<<<<<<<<<<<  |  TOWN |                          1
 0-----------------------------0------------------------------0
-1                         |   |   |                          1
-2                         |___|___|>>>>>>>>>>>>>>>>>>        2
-3                          \/ |                    \/        3
-4                          \/ |                    \/        4
-5                          \/ |                    \/        5
-6 ______(4)                \/ |                    \/_______ 6
-7 PEWTER   |               \/ |                   |          7
-8 CITY      <<<<<<<<<<<<<<<\/ |                (5)|LAVENDER  8
-9          |                  |                   |TOWN      9
-10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0  1  2  3  4  5  6  7  8  9  10


)" << endl << endl;
	cout << "type in the number you want to start in what region you want to be in" << endl;
	cin >> starterLocation;
	system("cls");
	if (starterLocation == 1)
	{
		cout << "Welcome to the region of MT.Moon" << endl;
		x = -10;
		y = 10;
	}
	else if (starterLocation == 2)
	{
		cout << "Welcome to the region of Veridian City" << endl;
		x = 10;
		y = 10;
	}
	else if (starterLocation == 3)
	{
		cout << "Welcome to the region of Pallet Town" << endl;
		x = 0;
		y = 0;
	}
	else if (starterLocation == 4)
	{
		cout << "Welcome to the region of Pewter City" << endl;
		x = -10;
		y = -10;
	}
	else if (starterLocation == 5)
	{
		cout << "Welcome to the region of Lavender City" << endl;
		x = 10;
		y = -10;
	}
	cout << "Your Journey Begins!!" << endl;
	system("pause");
	system("cls");

	while (OFF == 1)
	{
		cout << "Your current location is " << x << ", " << y << endl;
		cout << "What would you like to do?" << endl;
		cout << "[1] - Move" << endl;
		cout << "[2] - Pokemons" << endl;
		cout << "[3] - Look at the Map" << endl;
		if (x <= -7 && y >= 7)
		{
			cout << "[4] - Mt.Moon Pokemon Center" << endl;
		}
		else if (x >= 7 && y >= 7)
		{
			cout << "[4] - Veridian City Pokemon Center" << endl;
		}
		else if ((x <= 2 && x >= -2) && (y <= 2 && y >= -2))
		{
			cout << "[4] - Pallet Town Pokemon Center" << endl;
		}
		else if (x <= -7 && y <= -7)
		{
			cout << "[4] - Pewter City Pokemon Center" << endl;
		}
		else if (x >= 7 && y <= -7)
		{
			cout << "[4] - Lavender Town Pokemon Center" << endl;
		}
		cout << "[5] - End Game" << endl;
		cin >> input;
		system("cls");

		if (input == 1)
		{
			encounterrate = rand() % 100 + 1;
			if (encounterrate <= 30)
			{
				Pokemon->encounter(Pokemon->wildpokemonstat, 1);
				system("pause");
				system("cls");
			}

			cout << "Where do you want to move?" << endl << endl;
			cout << "[w] to move north" << endl;
			cout << "[a] to move west" << endl;
			cout << "[s] to move south" << endl;
			cout << "[d] to move east" << endl;
			cin >> moving;
			system("cls");

			if (moving == 'w')
			{
				cout << "You moved north.." << endl;
				y++;
			}
			else if (moving == 'a')
			{
				cout << "You moved west.." << endl;
				x--;
			}
			else if (moving == 's')
			{
				cout << "You moved south.." << endl;
				y--;
			}
			else if (moving == 'd')
			{
				cout << "You moved east.." << endl;
				x++;
			}
		}
		else if (input == 2)
		{
			for (int i = 0; i < pokemonintnum; i++)
			{
				cout << pokemonlist[i] << endl;
			}
		}
		else if (input == 3)
		{
			cout << R"(-10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0  1  2  3  4  5  6  7  8  9  10
 9    MT   |                   |                    |VERIDIAN 9
 8  MOON   |                  | /\>>>>>>>>>>>>>>>>>    CITY  8
 7 ____  _(1)                  | /\                 (2)______ 7
 6     /\                      | /\                           6
 5     /\                      | /\                           5
 4     /\                      | /\                           4
 3     /\                  (3)_|_/\                           3
 2     /\                  | PALLET|                          2
 1     <<<<<<<<<<<<<<<<<<  |  TOWN |                          1
 0-----------------------------0------------------------------0
-1                         |   |   |                          1
-2                         |___|___|>>>>>>>>>>>>>>>>>>        2
-3                          \/ |                    \/        3
-4                          \/ |                    \/        4
-5                          \/ |                    \/        5
-6 ______(4)                \/ |                    \/_______ 6
-7 PEWTER   |               \/ |                   |          7
-8 CITY      <<<<<<<<<<<<<<<\/ |                (5)|LAVENDER  8
-9          |                  |                   |TOWN      9
-10 -9 -8 -7 -6 -5 -4 -3 -2 -1 0  1  2  3  4  5  6  7  8  9  10


)" << endl << endl;
		}
		else if (input == 4)
		{
			
		}
		else if (input == 5)
		{
			OFF++;
			break;
		}
			system("pause");
			system("cls");
	}
}