#include "pokemon.h"
#include <time.h>
#include "trainer.h"

pokemon::pokemon()
{
	this->name = "";
	this->basehp = 0;
	this->hp = 0;
	this->lvl = 0;
	this->basedmg = 0;
	this->xp = 0;
	this->xptonextlevel = 0;
}

pokemon::pokemon(string name, int basehp, int hp, int lvl, int basedmg, int xp, int xptonextlevel)
{
	this->name = name;
	this->basehp = basehp;
	this->hp = hp;
	this->lvl = lvl;
	this->basedmg = basedmg;
	this->xp = xp;
	this->xptonextlevel = xptonextlevel;
}

void pokemon::getlist(pokemon*& wildpokemonstat, int& i)
{
	srand(time(NULL));
	int lvl = rand() % 10 + 1;
	pokemon* pokemonlist[15];

	this->pokemonlist[0] = new pokemon("Bulbasaur", 50, 50, lvl, 10, 15, 100);
	this->pokemonlist[1] = new pokemon("Charmander", 60, 60, lvl, 8, 14, 100);
	this->pokemonlist[2] = new pokemon("Squirtle", 50, 50, lvl, 11, 13, 100);
	this->pokemonlist[3] = new pokemon("Pidgey", 50, 50, lvl, 10, 15, 100);
	this->pokemonlist[4] = new pokemon("Rattata", 60, 60, lvl, 12, 13, 120);
	this->pokemonlist[5] = new pokemon("Spearow", 40, 40, lvl, 11, 14, 100);
	this->pokemonlist[6] = new pokemon("Ekans", 50, 50, lvl, 10, 15, 110);
	this->pokemonlist[7] = new pokemon("Pikachu", 40, 40, lvl, 15, 15, 120);
	this->pokemonlist[8] = new pokemon("Sandsfrew", 45, 45, lvl, 12, 13, 110);
	this->pokemonlist[9] = new pokemon("Nidoran", 50, 50, lvl, 11, 14, 100);
	this->pokemonlist[10] = new pokemon("Clefairy", 55, 55, lvl, 9, 16, 100);
	this->pokemonlist[11] = new pokemon("Vulpix", 60, 60, lvl, 8, 12, 95);
	this->pokemonlist[12] = new pokemon("Jigglypuff", 50, 50, lvl, 10, 15, 100);
	this->pokemonlist[13] = new pokemon("Zubat", 40, 40, lvl, 10, 16, 90);
	this->pokemonlist[14] = new pokemon("Meowth", 45, 45, lvl, 11, 9, 110);

	wildpokemonstat = this->pokemonlist[i];
}

void pokemon::showpokemon(pokemon *& pokemonlist)
{
	if (wildpokemonstat->lvl > 1)
	{
		levelUp(wildpokemonstat);
	}

	cout << this->wildpokemonstat->name << "'s stats" << endl << endl;
	cout << "Level: " << this->wildpokemonstat->lvl << endl;
	cout << "Health Points: " << this->wildpokemonstat->hp << "/" << this->wildpokemonstat->basehp << endl;
	cout << "Damage: " << this->wildpokemonstat->basedmg << endl;
	cout << "Exp: " << this->wildpokemonstat->xp << "/" << this->wildpokemonstat->xptonextlevel;
}

void pokemon::levelUp(pokemon *& pokemonlist)
{
	this->wildpokemonstat->basehp += ((wildpokemonstat->basehp * 0.15) * wildpokemonstat->lvl);
	this->wildpokemonstat->hp = wildpokemonstat->basehp;
	this->wildpokemonstat->basedmg += ((wildpokemonstat->basedmg * 0.10) * wildpokemonstat->lvl);
	this->wildpokemonstat->xp = 0;
	this->wildpokemonstat->xptonextlevel += ((wildpokemonstat->xptonextlevel * 0.20) * wildpokemonstat->lvl);
}

void pokemon::encounter(pokemon* wildpokemonstat, int r, int i)
{
	cout << "You have encountered a Wild " << pokemonlist[r]->name << endl;
	cout << "Level: " << lvl << endl;
	cout << "HP: " << hp << endl; 
	cout << "Damage: " << basedmg << endl;
	cout << "XP: " << xp << endl << endl;
	cout << "What would you like to do?" << endl;
	cout << "[5] Battle      [6] Capture     [7] Run" << endl;
	cin >> choice;
	if (choice == 5)
	{

	}
	else if (choice == 6)
	{
		for (int i = 1; i < pokemonint; i++)
		{
			pokemonint++;
			pokemonint[i] = pokemonlist[r]->name;
		}
	}
	else if (choice == 7)
	{
		cout << "You ran away!" << endl;
	}
}

void pokemon::battle(int hp, int lvl, int dmg, int encounter, int xp, int pokemonintnum)
{
	for (int i = 0; i < lvl; i++)
	{
		hp = hp + (hp * 0.15);
		dmg = dmg + (dmg * 0.10);
		xp = xp + (xp * 0.20);
	}
	for (int i = 0; i < 5; i++) //5 because thats the starting lvl
	{
		hp = hp + (hp * 0.15);
		dmg = dmg + (dmg * 0.10);
		xp = xp + (xp * 0.20);
	}

	trainer::pokemonint[6];
	for (int i = 0; i < 6; i++)
	{
		do
		{
			cout << pokemonint[i] << "dealt " << dmg << " damage  " << endl;
			cout << pokemonlist[r]->name << "dealt " << dmg << " damage  " << endl;
			hp = hp - dmg;
			hp = hp - dmg;
			cout << "Level : " << lvl << endl;
			cout << pokemonint[i] << endl;
			cout << "HP : " << hp << endl;
			cout << "Base Damage:" << dmg << endl;
			cout << "Experience points :" << xp << "/" << xp << endl;

			cout << "Level : " << lvl << endl;
			cout << pokemonlist[r]->name << endl;
			cout << "HP : " << hp << endl;
			cout << "Base Damage:" << dmg << endl;
			cout << "Experience points :" << xp << "/" << xp << endl;
			system("pause");
			system("cls");
			if (hp <= 0)
			{
				pokemonintnum++;
			}
		} while (hp > 0);
		if (hp <= 0 && pokemonintnum == 7)
		{
			cout << "You ran out of Pokemons and the Wild Pokemon killed you. " << endl;
			cout << "Game over" << endl;
		}
	}
}