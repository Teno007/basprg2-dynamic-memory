#pragma once

#include <string>
#include "pokemon.h"
#include <vector>
#include <iostream>

using namespace std;

class trainer
{
public:
	trainer();
	trainer(string name);
	void getName(string name);
	pokemon* pokemonint[6];
	int pokemonintnum = 0;
	string name;
	int x;
	int y;
	int starterChoice;
	string location;
	void move(int x, int y,int OFF, float encounterrate);
	int OFF = 1;
	int input;
	char moving;
	void starterpokemon();
	int starterLocation;
	bool safeLocation = true;
	vector<pokemon*> trainerCollection;
	pokemon* pokemonstat;
	pokemon* pokemonlist[15];
	float encounterrate;
};