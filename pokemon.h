#pragma once

#include <iostream>
#include <string>
#include <vector>

using namespace std;

class pokemon
{
public:
	pokemon();
	pokemon(string name, int basehp, int hp, int lvl, int basedmg, int xp, int xptonextlevel);
	string name;
	int basehp, hp, lvl, basedmg, xp, xptonextlevel;
	pokemon* wildpokemonstat;
	pokemon* pokemonlist[15];
	void getlist(pokemon*& pokemonlist, int& i);
	void showpokemon(pokemon*& pokemonlist);
	void levelUp(pokemon*& pokemonlist);
	void encounter(pokemon* wildpokemonstat,int r, int i);
	int r;
	int choice;
	int pokemonintnum;
	int pokemonint;
	int i = 1;
	void battle(int hp, int lvl, int dmg, int encounter, int x, int pokemonintnum);
	void encounter(pokemon* wildpokemonstat, int r, int i)

};